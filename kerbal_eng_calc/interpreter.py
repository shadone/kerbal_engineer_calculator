import sys
import math
from collections import namedtuple

from parts import *
from planets import *
import deltav_graph
from deltav_graph import *
from rocket_science import *

for engine_name in ENGINES:
    engine_name_attr = engine_name.replace('-', '_')
    setattr(sys.modules[__name__], engine_name_attr, get_engine(engine_name))

for tank_name in TANKS:
    tank_name_attr = tank_name.replace('-', '_')
    setattr(sys.modules[__name__], tank_name_attr, get_tank(tank_name))

for planet_name in PLANETS:
    planet_name_attr = planet_name.replace('-', '_')
    setattr(sys.modules[__name__], planet_name_attr, get_planet(planet_name))

def help_commands():
    print('Special commands:')
    print('help_commands() # Print this list of commands\n')
    print('list_engines() # Lists all engines')
    print('list_tanks() # Lists fuel tanks')
    print('list_planets() # Moons included')
    print('list_nodes() # List all delta-v nodes\n')
    print('total_thrust(engines) # calculate thrust of a list of engines')
    print('total_mass(parts) # calculate total mass of parts (engines and tanks)')
    print('# twr given mass of rocket, all engines, and reference planet')
    print('total_twr(mass, engines, planet)')
    print('total_seconds(engines, tanks, atmo=False) # Seconds of fuel')
    print('total_isp(engines, atmo=False) # calculate total ISP of engines')
    print('# Calc total delta-v')
    print('total_dv(mass, engines, tanks, atmo=False)')
    print('# calculate total delta-v given stages [(engines, tanks),...]')
    print('asparagus_dv(mass, engine_tank_pairs, atmo=False)') 
    print('calc_twr(mass, thrust, gravity) # Calculate TWR given float values')

def list_engines():
    for name in ENGINES:
        name_attr = name.replace('-', '_')
        print name_attr

def list_tanks():
    for name in TANKS:
        name_attr = name.replace('-', '_')
        print name_attr

def list_planets():
    for name in PLANETS:
        name_attr = name.replace('-', '_')
        print name_attr

def list_nodes():
    for attr in dir(deltav_graph):
        if not attr.startswith('dv_'):
            continue
        print attr

help_commands()
