#! /usr/bin/env python

import json
from parts import *
from bs4 import BeautifulSoup
import urllib2
url = urllib2.urlopen('http://wiki.kerbalspaceprogram.com/wiki/Parts#Liquid_Fuel_Engines')
text = url.read()
soup = BeautifulSoup(text)

def get_next(el, n):
    e = el
    for i in range(n):
        e = e.next_element
    return e
    
def get_stats_tank(vals, title):
    return {
            NAME: title,
            MOUNT: vals[0],
            WET_MASS: float(vals[1]),
            DRY_MASS: float(vals[2]),
            LIQ_FUEL: float(vals[4]),
            OXI_FUEL: float(vals[5]),
            FUEL_MASS: float(vals[1]) - float(vals[2]),
    }

def get_stats_engine(vals, title):
    try:
        tvc = float(vals[12])
    except ValueError:
        tvc = 0.0
    return {
            NAME: title,
            MOUNT: vals[0],
            MASS: float(vals[2]),
            THRUST: float(vals[6]),
            ATM_TS: float(vals[8]),
            VAC_TS: float(vals[9]),
            ATM_ISP: float(vals[10]),
            VAC_ISP: float(vals[11]),
            TVC: tvc,
    }


def get_stats(title, typ):
    print('Parsing %s' % title)
    if title == 'R.A.P.I.E.R. Engine':
        off = 3
    else:
        off = 0
    eng = soup.find_all('a', title=title)[0]
    if typ == 'engines':
        max_col = 12
    elif typ == 'tanks':
        max_col = 5
    vals = [get_next(eng, 5 + off + (3*i)).strip() for i in range(max_col+1)]
    if typ == 'engines':
        return get_stats_engine(vals, title)
    elif typ == 'tanks':
        return get_stats_tank(vals, title)
            
PARTS = {
    'engines': {
        'lv-1r': {NAME: 'LV-1R Liquid Fuel Engine'},
        '24-77': {NAME: 'Rockomax 24-77'},
        'mark55': {NAME: 'Rockomax Mark 55 Radial Mount Liquid Engine'},
        'lv-1': {NAME: 'LV-1 Liquid Fuel Engine'},
        '48-7s': {NAME: 'Rockomax 48-7S'},
        'lv-t30': {NAME: 'LV-T30 Liquid Fuel Engine'},
        'lv-t45': {NAME: 'LV-T45 Liquid Fuel Engine'},
        'lv-909': {NAME: 'LV-909 Liquid Fuel Engine'},
        'rapier': {NAME: 'R.A.P.I.E.R. Engine'},
        'aerospike': {NAME: 'Toroidal Aerospike Rocket'},
        'poodle': {NAME: 'Rockomax "Poodle" Liquid Engine'},
        'mainsail': {NAME: 'Rockomax "Mainsail" Liquid Engine'},
        'skipper': {NAME: 'Rockomax "Skipper" Liquid Engine'},
        'lv-n': {NAME: 'LV-N Atomic Rocket Motor'},
    },
    'tanks': {
        'toroidal': {NAME: 'ROUND-8 Toroidal Fuel Tank'},
        'oscar-b': {NAME: 'Oscar-B Fuel Tank'},
        'fl-t100': {NAME: 'FL-T100 Fuel Tank'},
        'fl-t200': {NAME: 'FL-T200 Fuel Tank'},
        'fl-t400': {NAME: 'FL-T400 Fuel Tank'},
        'fl-t800': {NAME: 'FL-T800 Fuel Tank'},
        'x200-8': {NAME: 'Rockomax X200-8 Fuel Tank'},
        'x200-16': {NAME: 'Rockomax X200-16 Fuel Tank'},
        'x200-32': {NAME: 'Rockomax X200-32 Fuel Tank'},
        'x200-64': {NAME: 'Rockomax Jumbo-64 Fuel Tank'},
    },
}

for typ in PARTS.keys():
    for part in PARTS[typ].keys():
        PARTS[typ][part] = get_stats(PARTS[typ][part][NAME], typ)

with open('parts.json', 'w') as f:
    json.dump(PARTS, f, indent=4)

