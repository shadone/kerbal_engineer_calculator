#!/usr/bin/env python
from planets import get_planet
from collections import namedtuple

# 1.7 TWR optimal for launching from atmosphere:
# http://forum.kerbalspaceprogram.com/threads/63501-What-is-the-optimal-Thrust-to-Weight-ratio-for-launching-space-travel-and-landing

PathNode = namedtuple('PathNode', 'dvnode cost twr ref')
StageNode = namedtuple('StageNode', 'pathnode orig dest atmo')

class DVNode(object):
    
    __DIRECT = {}
    KERBOL_TWR = 0.1
    ORBIT_TWR = 0.5
    ASCENT_TWR = 1.7
    DESCENT_TWR = 2.0

    def __repr__(self):
        return 'dv_%s' % self.name

    def __init__(self, name, planet=None, orbit=None,
        **neighbors_kwargs):
        self.name = name
        self.neighbors = {}
        self.planet = planet
        self.orbit = orbit
        for node_name, cost in neighbors_kwargs.items():
            node = DVNode.get(node_name)
            if isinstance(cost, tuple):
                self.neighbors[node] = cost[0]
                node.add(cost[1], self)
            else:
                self.neighbors[node] = cost
                node.add(cost, self)
        DVNode.__DIRECT[name] = self

    def add(self, cost, node):
        self.neighbors[node] = cost

    @classmethod
    def get(cls, name):
        return cls.__DIRECT[name]
    
    def brute_paths(self, target, chutes=True):
        paths = []
        self._brute_paths(target, [], paths, chutes=chutes)
        paths.sort()
        return paths

    def _brute_paths(self, target, path, paths, chutes=True):
        if self is target:
            # node, cost, min TWR
            path += [PathNode(self, 0, 0.0, None)]
            costs = [x.cost for x in path]
            paths += [(sum(costs), path)]
            return
        dvnodes = [x.dvnode for x in path]
        for node, ncost in self.neighbors.items():
            if node in dvnodes:
                continue
            cost = ncost
            if (
                cost == 0 and not (
                    chutes and 
                    target.planet and 
                    target.planet.atmo)):
                cost = node.neighbors[self]
            if self.planet:
                min_twr = DVNode.ASCENT_TWR
                ref = self
            elif node.planet and chutes and node.planet.atmo:
                min_twr = DVNode.KERBOL_TWR
                ref = node
            elif node.planet:
                min_twr = DVNode.DESCENT_TWR
                ref = node
            elif node.orbit == dv_kerbol or self.orbit == dv_kerbol:
                min_twr = DVNode.KERBOL_TWR
                ref = self.orbit
            else:
                min_twr = DVNode.ORBIT_TWR
                ref = node.orbit
            path2 = path[:] + [PathNode(self, cost, min_twr, ref)]
            node._brute_paths(target, path2, paths, chutes=chutes)

dv_kerbol = DVNode('kerbol', planet=get_planet('kerbol'))
dv_kerbin = DVNode('kerbin', planet=get_planet('kerbin'))
dv_kerbin_lo = DVNode('kerbin_lo', orbit=dv_kerbin,
    kerbin=(0, 4500))
dv_keo_txfr = DVNode('keo_txfr', orbit=dv_kerbin,
    kerbin_lo=(0, 680))
dv_keo = DVNode('keo', orbit=dv_kerbin,
    keo_txfr=435)
dv_mun_txfr = DVNode('mun_txfr', orbit=dv_kerbin,
    keo_txfr=(0, 180))
dv_mun = DVNode('mun', planet=get_planet('mun'))
dv_mun_lo = DVNode('mun_lo', orbit=dv_mun,
    mun=580)
dv_mun_esc = DVNode('mun_esc', orbit=dv_mun,
    mun_lo=230, mun_txfr=80)
dv_minmus = DVNode('minmus', planet=get_planet('minmus'))
dv_minmus_lo = DVNode('minmus_lo', orbit=dv_minmus,
    minmus=180)
dv_minmus_esc = DVNode('minmus_esc', orbit=dv_minmus,
    minmus_lo=70)
dv_minmus_txfr = DVNode('minmus_txfr', orbit=dv_kerbin,
    mun_txfr=(0,70), minmus_esc=90)
dv_kerbin_esc = DVNode('kerbin_esc', orbit=dv_kerbin,
    minmus_txfr=(0,20))
dv_eve = DVNode('eve', planet=get_planet('eve'))
dv_eve_lo = DVNode('eve_lo', orbit=dv_eve,
    eve=(0,12000))
dv_gilly = DVNode('gilly', planet=get_planet('gilly'))
dv_gilly_lo = DVNode('gilly_lo', orbit=dv_gilly,
    gilly=30)
dv_gilly_esc = DVNode('gilly_esc', orbit=dv_gilly,
    gilly_lo=10)
dv_gilly_txfr = DVNode('gilly_txfr', orbit=dv_eve,
    gilly_esc=450, eve_lo=(0,1270))
dv_eve_esc = DVNode('eve_esc', orbit=dv_eve,
    gilly_txfr=(0,80))
dv_eve_txfr = DVNode('eve_txfr', orbit=dv_kerbol,
    eve_esc=(0,80), kerbin_esc=(0,90))
dv_duna = DVNode('duna', planet=get_planet('duna'))
dv_duna_lo = DVNode('duna_lo', orbit=dv_duna,
    duna=(0,1300))
dv_ike = DVNode('ike', planet=get_planet('ike'))
dv_ike_lo = DVNode('ike_lo', orbit=dv_ike,
    ike=390)
dv_ike_esc = DVNode('ike_esc', orbit=dv_ike,
    ike_lo=150)
dv_ike_txfr = DVNode('ike_txfr', orbit=dv_duna,
    duna_lo=(0,330), ike_esc=30)
dv_duna_esc = DVNode('duna_esc', orbit=dv_duna,
    ike_txfr=(0,30))
dv_duna_txfr = DVNode('duna_txfr', orbit=dv_kerbol,
    duna_esc=(0,250), kerbin_esc=(0,140))
dv_dres = DVNode('dres', planet=get_planet('dres'))
dv_dres_lo = DVNode('dres_lo', orbit=dv_dres,
    dres=430)
dv_dres_esc = DVNode('dres_esc', orbit=dv_dres,
    dres_lo=150)
dv_dres_txfr = DVNode('dres_txfr', orbit=dv_kerbol,
    dres_esc=1140, duna_txfr=480)
dv_jool = DVNode('jool', planet=get_planet('jool'))
dv_jool_lo = DVNode('jool_lo', orbit=dv_jool,
    jool=(0, 22000))
dv_laythe = DVNode('laythe', planet=get_planet('laythe'))
dv_laythe_lo = DVNode('laythe_lo', orbit=dv_laythe,
    laythe=(0, 3200))
dv_laythe_esc = DVNode('laythe_esc', orbit=dv_laythe,
    laythe_lo=(0, 780))
dv_laythe_txfr = DVNode('laythe_txfr', orbit=dv_jool,
    laythe_esc=(0, 290), jool_lo=(0, 1880))
dv_vall = DVNode('vall', planet=get_planet('vall'))
dv_vall_lo = DVNode('vall_lo', orbit=dv_vall,
    vall=860)
dv_vall_esc = DVNode('vall_esc', orbit=dv_vall,
    vall_lo=330)
dv_vall_txfr = DVNode('vall_txfr', orbit=dv_jool,
    vall_esc=580, laythe_txfr=(0, 310))
dv_tylo = DVNode('tylo', planet=get_planet('tylo'))
dv_tylo_lo = DVNode('tylo_lo', orbit=dv_tylo,
    tylo=2270)
dv_tylo_esc = DVNode('tylo_esc', orbit=dv_tylo,
    tylo_lo=860)
dv_tylo_txfr = DVNode('tylo_txfr', orbit=dv_jool,
    tylo_esc=240, vall_txfr=(0, 220))
dv_bop = DVNode('bop', planet=get_planet('bop'))
dv_bop_lo = DVNode('bop_lo', orbit=dv_bop,
    bop=220)
dv_bop_esc = DVNode('bop_esc', orbit=dv_bop,
    bop_lo=70)
dv_bop_txfr = DVNode('bop_txfr', orbit=dv_jool,
    bop_esc=830, tylo_txfr=(0, 180))
dv_pol = DVNode('pol', planet=get_planet('pol'))
dv_pol_lo = DVNode('pol_lo', orbit=dv_pol,
    pol=130)
dv_pol_esc = DVNode('pol_esc', orbit=dv_pol,
    pol_lo=50)
dv_pol_txfr = DVNode('pol_txfr', orbit=dv_jool,
    pol_esc=770, tylo_txfr=(0, 60))
dv_jool_esc = DVNode('jool_esc', orbit=dv_jool,
    pol_txfr=(0, 160))
dv_jool_txfr = DVNode('jool_txfr', orbit=dv_kerbol,
    jool_esc=(0, 160), dres_txfr=(0, 370))
dv_eeloo = DVNode('eeloo', planet=get_planet('eeloo'))
dv_eeloo_lo = DVNode('eeloo_lo', orbit=dv_eeloo,
    eeloo=620)
dv_eeloo_esc = DVNode('eeloo_esc', orbit=dv_eeloo,
    eeloo_lo=240)
dv_eeloo_txfr = DVNode('eeloo_txfr', orbit=dv_kerbol,
    eeloo_esc=1130, jool_txfr=(0, 160))
dv_kerbol_esc = DVNode('kerbol_esc', orbit=dv_kerbol,
    eeloo_txfr=(0, 650))
dv_moho = DVNode('moho', planet=get_planet('moho'))
dv_moho_lo = DVNode('moho_lo', orbit=dv_moho,
    moho=870)
dv_moho_esc = DVNode('moho_esc', orbit=dv_moho,
    moho_lo=320)
dv_moho_txfr = DVNode('moho_txfr', orbit=dv_kerbol,
    moho_esc=2090, eve_txfr=(0, 670))
# No Kerbol orbit/etc


def flatten_stages(orig_name, dest_name, chutes=False, compress=False,
    round_trip=False):
    ''' Used to determine how many different stages of the flight there are.
    '''
    stages = []
    orig = DVNode.get(orig_name)
    dest = DVNode.get(dest_name)
    path = orig.brute_paths(dest, chutes=chutes)[0][1]
    start_name = ''
    last_twr = None
    last_ref = None
    last_atmo = False
    atmo = False
    accum_cost = 0
    for i, node1 in enumerate(path[:-1]):
        node2 = path[i+1]
        cost = node1.cost
        twr = node1.twr
        if node1.dvnode.planet is not None:
            atmo = node1.dvnode.planet.atmo
        else:
            atmo = False
        if node2.ref == dv_kerbol:
            ref = dv_kerbol
        else:
            ref = node1.ref
        if last_twr == twr and ref == last_ref:
            accum_cost += cost
        elif compress and not atmo and (
            i > 0 and
            last_twr <= DVNode.ORBIT_TWR and 
            twr <= DVNode.ORBIT_TWR):
            twr1 = last_twr * last_ref.planet.grav
            twr2 = twr * ref.planet.grav
            twr, planet_node = max(
                (twr1, last_ref), 
                (twr2, ref))
            twr /= planet_node.planet.grav
            accum_cost += cost
            ref = planet_node.planet
        else:
            if accum_cost != 0:
                pnode = PathNode(None, accum_cost, last_twr, last_ref)
                stages += [StageNode(
                    pnode, 
                    start_name, 
                    node1.dvnode.name,
                    last_atmo
                )]
            accum_cost = cost
            start_name = node1.dvnode.name
            last_twr = twr
            last_ref = ref
            last_atmo = atmo
    if accum_cost != 0:
        pnode = PathNode(None, accum_cost, last_twr, last_ref)
        stages += [StageNode(
            pnode,
            start_name,
            node2.dvnode.name,
            node2.dvnode.planet is not None and node2.dvnode.planet.atmo,
        )]
    if round_trip:
        ret_stages = flatten_stages(dest_name, orig_name, chutes=chutes,
            compress=compress)
        stages += ret_stages
    return stages

def output_stages(stages):
    ''' Spit out stages for debugging '''
    ref = None
    total_dv = 0
    print('%10s\t%4s\t%10s\t%10s\t%10s\t%10s' % (
        'Delta-V', 'TWR', 'Reference', 
        'Start', 'End', 'Atmosphere'))
    for pnode, orig, dest, atmo in stages:
        print('%10s\t%2.2f\t%10s\t%10s\t%10s\t%10s' % (
            pnode.cost, pnode.twr, 
            pnode.ref.name, orig, dest,
            str(atmo),
        ))
        total_dv += pnode.cost
    print('='*10)
    print('%10s' % total_dv)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('origin')
    parser.add_argument('destination')
    parser.add_argument('--parachutes', '-p', action='store_true')
    parser.add_argument('--compress', '-c', action='store_true')
    parser.add_argument('--round-trip', '-r', action='store_true')
    args = parser.parse_args()
    stages = flatten_stages(args.origin, args.destination,
        chutes=args.parachutes, compress=args.compress, 
        round_trip=args.round_trip)
    output_stages(stages)
