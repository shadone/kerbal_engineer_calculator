GRAV = 'gravity'
ORBIT = 'orbit'
ATMO = 'atmosphere'
ORBIT_DV = 'orbit delta-v'

PLANETS_D = {
    'dres': {
        ORBIT: 'kerbol',
        GRAV: 1.13,
        ATMO: False,
        ORBIT_DV: 430,
    },
    'duna': {
        ORBIT: 'kerbol',
        GRAV: 2.94,
        ATMO: True,
        ORBIT_DV: 1300,
    },
    'eeloo': {
        ORBIT: 'kerbol',
        GRAV: 1.69,
        ATMO: False,
        ORBIT_DV: 620,
    },
    'eve': {
        ORBIT: 'kerbol',
        GRAV: 16.7,
        ATMO: True,
        ORBIT_DV: 12000,
    },
    'jool': {
        ORBIT: 'kerbol',
        GRAV: 7.85,
        ATMO: True,
        ORBIT_DV: 22000,
    },
    'kerbin': {
        ORBIT: 'kerbol',
        GRAV: 9.81,
        ATMO: True,
        ORBIT_DV: 4500,
    },
    'moho': {
        ORBIT: 'kerbol',
        GRAV: 2.70,
        ATMO: False,
        ORBIT_DV: 870,
    },
    'bop': {
        ORBIT: 'jool',
        GRAV: 0.589,
        ATMO: False,
        ORBIT_DV: 220,
    },
    'gilly': {
        ORBIT: 'eve',
        GRAV: 0.049,
        ATMO: False,
        ORBIT_DV: 30,
    },
    'ike': {
        ORBIT: 'duna',
        GRAV: 1.10,
        ATMO: False,
        ORBIT_DV: 390,
    },
    'laythe': {
        ORBIT: 'jool',
        GRAV: 7.85,
        ATMO: True,
        ORBIT_DV: 3200,
    },
    'mun': {
        ORBIT: 'kerbin',
        GRAV: 1.63,
        ATMO: False,
        ORBIT_DV: 580,
    },
    'minmus': {
        ORBIT: 'kerbin',
        GRAV: 0.491,
        ATMO: False,
        ORBIT_DV: 180,
    },
    'pol': {
        ORBIT: 'jool',
        GRAV: 0.373,
        ATMO: False,
        ORBIT_DV: 130,
    },
    'tylo': {
        ORBIT: 'jool',
        GRAV: 7.85,
        ATMO: False,
        ORBIT_DV: 2270,
    },
    'vall': {
        ORBIT: 'jool',
        GRAV: 2.31,
        ATMO: False,
        ORBIT_DV: 860,
    },
    'kerbol': {
        ORBIT: None,
        GRAV: 17.1,
        ATMO: False,
        ORBIT_DV: 66940,
    },
}

class Planet(object):
    def __repr__(self):
        return '%s (%.2f G, %d DV)' % (
            self.name, self.grav, self.orbit_dv)

    def decode(self, name, dct):
        self.orbit = dct[ORBIT]
        self.grav = dct[GRAV]
        self.atmo = dct[ATMO]
        self.orbit_dv = dct[ORBIT_DV]
        self.name = name

PLANETS = {}
for name, data in PLANETS_D.items():
    planet = Planet()
    planet.decode(name, data)
    PLANETS[name] = planet

def get_planet(planet_name):
    return PLANETS[planet_name]

def get_moon(moon_name):
    return PLANETS[moon_name]

