#!/usr/bin/env python

import sys

from rocket_science import Combo, target, print_combo
from planets import get_planet, ORBIT_DV, ATMO
from deltav_graph import DVNode, flatten_stages, output_stages

def debug(msg, node=None):
    print('[DEBUG] %s' % msg)
    if node is not None:
        print_combo(node.combo)

class Node(object):

    def __init__(self, combo, stage, dv, parent, children):
        self.combo = combo
        self.stage = stage
        self.children = children
        self.parent = parent
        self.dv = dv
        self.dead = False


def best_combos(mass, stage):
    ''' Get up to 3 combos, higher priority over those that complete the dv requirement
    '''
    combos = target(mass, stage)
    if not combos:
        return []
    return combos[:5]

def branch_out(node, stage, dv):
    combos = best_combos(node.combo.ship_mass, stage)
    if not combos:
        #debug("Killing: %s" % str(node), node=node)
        node.dead = True
        parent = node.parent
        while parent and len(parent.children) == 1:
            parent.dead = True
            parent = parent.parent
        return None
    children = []
    for combo in combos:
        if node.stage == stage:
            children += [Node(combo, stage, node.dv + combo.dv, node, [])]
        else:
            children += [Node(combo, stage, combo.dv, node, [])]
    node.children = children
    return node

def debug_branch(stage, env, dv, root):
    print("*"*80)
    print("BRANCHING")
    print("Stage: %s\nEnv: %s" % (stage, env.name)) 
    print("Root Stage: %s\nDV to go: %s" % (root.stage, dv))

def grow_tree(root, stage):
    ''' Expand on the leaves once '''
    dv = stage.pathnode.cost
    if not root.children:
        if root.stage == stage and root.dv < dv:
            branch_out(root, stage, dv - root.dv)
        elif root.stage != stage:
            branch_out(root, stage, dv) 
    for node in root.children:
        grow_tree(node, stage)
    return root

def calc_tree(mass, orig_name, dest_name, chutes=True, round_trip=False):
    stages = flatten_stages(orig_name, dest_name, compress=True, 
        chutes=chutes, round_trip=round_trip)[::-1]
    payload = Node(Combo(0, 0, 0, 0, mass, 0, [], [], False), 'lander', 0, None, [])
    tree = payload
    for stage in stages:
        grow_tree(tree, stage)
    return tree
         
def print_path(path):
    for node in path[1:]:
        print_combo(node.combo)

def print_paths(root):
    for path in build_paths(root, []):
        print_path(path)
        print('*'*80)

def build_paths(node, path):
    if node.dead:
        return
    path += [node]
    if not node.children:
        yield path
    for child in node.children:
        for new_path in build_paths(child, path[:]):
            yield new_path

def calc_path_fitnesses(root):
    paths = []
    for path in build_paths(root, []):
        fitness = path[-1].combo.ship_mass
        paths += [(fitness, path)]
    paths.sort()
    return paths

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('mass', type=float,
        help='Mass of payload')
    parser.add_argument('orig', 
        help='starting location (kerbin, mun_lo, ike)')
    parser.add_argument('dest', 
        help='ending location (kerbin_lo, mun, eve)')
    parser.add_argument('--parachutes', '-p', action='store_true', 
        help='Use parachutes when landing on atmospheric body.')
    parser.add_argument('--round-trip', '-r', action='store_true',
        help='Round triiiip!')
    parser.add_argument('--journey', '-j', action='store_true',
        help='Describe journey to destination, TWR and reference delta-v')
    args = parser.parse_args()

    if args.journey:
        stages = flatten_stages(args.orig, args.dest, compress=True,
            round_trip=args.round_trip)
        output_stages(stages)
        sys.exit(0)

    tree = calc_tree(
        args.mass,
        args.orig,
        args.dest,
        chutes=args.parachutes,
        round_trip=args.round_trip,
    )
    paths = calc_path_fitnesses(tree)
    for fit, path in paths:
        print('Fitness: %f' % fit)
        print_path(path)
        print('*'*80)
    
